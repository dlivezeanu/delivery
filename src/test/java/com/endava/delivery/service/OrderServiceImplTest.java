package com.endava.delivery.service;


import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;


import com.endava.delivery.entity.DeliveryManEntity;
import com.endava.delivery.entity.OrderEntity;
import com.endava.delivery.model.Order;
import com.endava.delivery.model.Status;
import com.endava.delivery.repository.DeliveryManRepository;
import com.endava.delivery.repository.OrderRepository;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

@ExtendWith(MockitoExtension.class)
public class OrderServiceImplTest {

  @InjectMocks
  private OrderServiceImpl orderServiceImpl;
  @Mock
  private OrderRepository orderRepository;
  @Mock
  private DeliveryManRepository deliveryManRepository;
  @Mock
  private MongoTemplate mongoTemplate;


  @BeforeEach
  public void init() {
    orderServiceImpl = new OrderServiceImpl(orderRepository, mongoTemplate, deliveryManRepository);
  }

  @Test
   void setOrderStatusToReadyTest() {

    UUID uuid = UUID.randomUUID();
    OrderEntity orderEntity = buildOrder(uuid);

    when(orderRepository.findById(any())).thenReturn(Optional.of(orderEntity));
    when(orderRepository.save(any())).thenReturn(orderEntity);

    Optional<OrderEntity> result = orderServiceImpl.setOrderStatusToReady(uuid);

    assertNotNull(result);
    assertEquals(Status.PENDING_DELIVERY, result.get().getStatus());
  }

  @Test
   void setOrderStatusToDeliveredTest() {

    UUID uuid = UUID.randomUUID();
    OrderEntity orderEntity = buildOrder(uuid);

    when(orderRepository.findById(any())).thenReturn(Optional.of(orderEntity));
    when(orderRepository.save(any())).thenReturn(orderEntity);

    Optional<OrderEntity> result = orderServiceImpl.setOrderStatusToDelivered(uuid);

    assertNotNull(result);
    assertEquals(Status.DELIVERED, result.get().getStatus());
  }

  @Test
   void setOrderStatusToReadyOrderNotFoundTest() {

    when(orderRepository.findById(any())).thenReturn(Optional.empty());

    Optional<OrderEntity> result = orderServiceImpl.setOrderStatusToReady(UUID.randomUUID());

    assertNotNull(result);
  }

  @Test
   void setOrderStatusToDeliveredOrderNotFoundTest() {

    when(orderRepository.findById(any())).thenReturn(Optional.empty());

    Optional<OrderEntity> result = orderServiceImpl.setOrderStatusToDelivered(UUID.randomUUID());

    assertNotNull(result);
  }

  @Test
   void insertOrderTest() {
    UUID uuid = UUID.randomUUID();
    OrderEntity orderEntity = buildOrder(uuid);
    when(orderRepository.insert(orderEntity)).thenReturn(orderEntity);

    assertEquals(orderServiceImpl.insertOrder(orderEntity), Optional.of(orderEntity));
  }

  @Test
   void updateOrderWithEtaTest() {
    //GIVEN
    UUID orderId = UUID.randomUUID();
    UUID deliveryManId = UUID.randomUUID();
    int estimatedTime = 30;
    OrderEntity orderEntity = buildOrder(orderId);

    //WHEN
    when(orderRepository.findById(any())).thenReturn(Optional.of(orderEntity));
    when(orderRepository.save(any())).thenReturn(orderEntity);
    Optional<OrderEntity> result =
        orderServiceImpl.updateOrderWithEta(orderId, estimatedTime, deliveryManId);

    //THEN
    assertNotNull(result);
  }

  @Test
   void updateOrderWithEtaNotFoundTest() {

    when(orderRepository.findById(any())).thenReturn(Optional.empty());

    Optional<OrderEntity> result =
        orderServiceImpl.updateOrderWithEta(UUID.randomUUID(), 20, UUID.randomUUID());

    assertNotNull(result);
  }

  @Test
   void getOrderEstimatedTimeTest() {
    UUID uuid = UUID.randomUUID();
    OrderEntity orderEntity = buildOrder(uuid);
    List<OrderEntity> listToReturn = new ArrayList<>();
    listToReturn.add(orderEntity);
    when(mongoTemplate.find(any(), eq(OrderEntity.class))).thenReturn(listToReturn);
    assertNotNull(orderServiceImpl.getOrderEstimatedTime(uuid));
  }

  @Test
   void getOrdersByStatusWhenDeliveryManExists() {
    UUID deliveryManUuid = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");
    List<OrderEntity> listToVerify = new ArrayList<>();
    OrderEntity orderEntity = buildOrder(UUID.randomUUID());
    orderEntity.setDeliveryManId(deliveryManUuid);
    listToVerify.add(orderEntity);
    DeliveryManEntity deliveryManEntity = buildDeliveryMan(deliveryManUuid);
    when(deliveryManRepository.findById(deliveryManUuid))
        .thenReturn(Optional.of(deliveryManEntity));
    when(mongoTemplate.find(any(), eq(OrderEntity.class))).thenReturn(listToVerify);
    assertEquals(orderServiceImpl.getOrdersByStatus(deliveryManUuid, "Pending preparation"),
        listToVerify);
    assertEquals(orderServiceImpl.getOrdersByStatus(deliveryManUuid, "Pending delivery"),
        listToVerify);
    assertEquals(orderServiceImpl.getOrdersByStatus(deliveryManUuid, null),
        listToVerify);
    assertEquals(orderServiceImpl.getOrdersByStatus(deliveryManUuid, "Delivered"),
        new ArrayList<>());
  }

  @Test
   void getOrdersByStatusWhenDeliveryManDoesNotExists() {
    UUID deliveryManUuid = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");
    assertEquals(orderServiceImpl.getOrdersByStatus(deliveryManUuid, "Pending preparation"),
        new ArrayList<>());
  }

  private DeliveryManEntity buildDeliveryMan(UUID uuid) {
    DeliveryManEntity deliveryManEntity = new DeliveryManEntity();
    deliveryManEntity.setId(uuid);
    deliveryManEntity.setArea("Central Zone");
    return deliveryManEntity;
  }

  private OrderEntity buildOrder(UUID uuid) {
    OrderEntity orderEntity = new OrderEntity();
    orderEntity.setId(uuid);
    orderEntity.setDeliveryAddress("address");
    orderEntity.setRestaurantAddress("restaurantAddress");
    orderEntity.setRestaurantArea("Central Zone");
    orderEntity.setRestaurantName("KFC");
    return orderEntity;
  }


}
