package com.endava.delivery;
import static org.junit.jupiter.api.Assertions.assertNotNull;


import com.endava.delivery.controller.DeliveryController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DeliveryApplicationTests {
  @Autowired
  private DeliveryController deliveryController;
  @Test
  void contextLoads()
  {
    assertNotNull(deliveryController);
  }

}
