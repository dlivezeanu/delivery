package com.endava.delivery.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import com.endava.delivery.entity.OrderEntity;
import com.endava.delivery.model.Order;
import com.endava.delivery.service.OrderService;
import com.endava.delivery.utils.Convertors;
import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
public class DeliveryControllerTest {

  private static MockMvc mockMvc;

  @InjectMocks
  private static DeliveryController deliveryController;
  @Mock
  private static OrderService orderService;
  @Mock
  private static RestTemplate restTemplate;
  @Mock
  private static Mapper mapper;

  @BeforeAll
  public static void init() {
    deliveryController = new DeliveryController(orderService, restTemplate);
    mockMvc = MockMvcBuilders.standaloneSetup(deliveryController).build();
    mapper = DozerBeanMapperBuilder.create().build();
  }

  Order order = buildOrder();
  OrderEntity orderEntity = buildOrderEntity();


  @Test
  void whenCreateOrderSuccessful() throws Exception {
    when(mapper.map(order, OrderEntity.class)).thenReturn(orderEntity);
    when(orderService.insertOrder(orderEntity)).thenReturn(Optional.of(orderEntity));

    String orderJSON = Convertors.convertObjectToJson(order);

    MvcResult result = mockMvc.perform(post("/delivery/order/pending")
        .content(orderJSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andReturn();
    MockHttpServletResponse response = result.getResponse();
    assertEquals(200, response.getStatus());
  }

  @Test
  void whenCreateOrderNotSuccessful() throws Exception {
    when(mapper.map(order, OrderEntity.class)).thenReturn(orderEntity);
    when(orderService.insertOrder(orderEntity)).thenReturn(Optional.empty());

    String orderJSON = Convertors.convertObjectToJson(order);

    MvcResult result = mockMvc.perform(post("/delivery/order/pending")
        .content(orderJSON)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is5xxServerError())
        .andReturn();
    MockHttpServletResponse response = result.getResponse();
    assertEquals(500, response.getStatus());
  }

  @Test
  void getDeliveryEstimatedTimeTestSuccessful() throws Exception {
    UUID id = UUID.randomUUID();
    when(orderService.getOrderEstimatedTime(id)).thenReturn(Optional.of(orderEntity));
    MvcResult result = mockMvc.perform(get("/delivery/order/" + id))
        .andExpect(status().isOk())
        .andReturn();
    MockHttpServletResponse response = result.getResponse();
    assertEquals(200, response.getStatus());
  }

  @Test
  void getDeliveryEstimatedTimeTestNotSuccessful() throws Exception {
    UUID id = UUID.randomUUID();
    when(orderService.getOrderEstimatedTime(id)).thenReturn(Optional.empty());
    MvcResult result = mockMvc.perform(get("/delivery/order/" + id))
        .andExpect(status().is4xxClientError())
        .andReturn();
    MockHttpServletResponse response = result.getResponse();
    assertEquals(400, response.getStatus());
  }
  @Test
  void setOrderStatusToReadyTestSuccessful() throws Exception {
    UUID id = UUID.randomUUID();
    OrderEntity orderEntity = buildOrderEntity();
    when(orderService.setOrderStatusToReady(id)).thenReturn(Optional.of(orderEntity));
    MvcResult result = mockMvc.perform(put("/delivery/order/ready/" + id))
        .andExpect(status().isOk())
        .andReturn();
    MockHttpServletResponse response = result.getResponse();
    assertEquals(200, response.getStatus());

  }

  @Test
  void setOrderStatusToDeliveredTestSuccessful() throws Exception {
    UUID orderId = UUID.randomUUID();
    OrderEntity orderEntity = buildOrderEntity();
    when(orderService.setOrderStatusToDelivered(orderId)).thenReturn(Optional.of(orderEntity));
    MvcResult result = mockMvc.perform(put("/delivery/order/delivered/" + orderId))
        .andExpect(status().isOk())
        .andReturn();
    MockHttpServletResponse response = result.getResponse();
    assertEquals(200, response.getStatus());

  }

  @Test
  void setOrderStatusToReadyTestNotSuccessful() throws Exception {
    UUID id = UUID.randomUUID();
    OrderEntity orderEntity = buildOrderEntity();
    when(orderService.setOrderStatusToReady(id)).thenReturn(Optional.empty());
    MvcResult result = mockMvc.perform(put("/delivery/order/ready/" + id))
        .andExpect(status().is4xxClientError())
        .andReturn();
    MockHttpServletResponse response = result.getResponse();
    assertEquals(404, response.getStatus());

  }

  @Test
  void setOrderStatusToDeliveredTestNotSuccessful() throws Exception {
    UUID orderId = UUID.randomUUID();
    OrderEntity orderEntity = buildOrderEntity();
    when(orderService.setOrderStatusToDelivered(orderId)).thenReturn(Optional.empty());
    MvcResult result = mockMvc.perform(put("/delivery/order/delivered/" + orderId))
        .andExpect(status().is4xxClientError())
        .andReturn();
    MockHttpServletResponse response = result.getResponse();
    assertEquals(404, response.getStatus());

  }

  @Test
  void listOrderToPickUpWhenSuccessful() throws Exception {


    UUID deliveryManId = UUID.randomUUID();
    String statusValue = "Pending preparation";
    List<OrderEntity> orderEntities = new ArrayList<>();
    OrderEntity orderEntity = buildOrderEntity();
    orderEntities.add(orderEntity);
    when(orderService.getOrdersByStatus(deliveryManId,statusValue)).thenReturn(orderEntities);
    MvcResult result = mockMvc.perform(get("/delivery/order/pickup/")
        .param("deliveryManId",deliveryManId.toString())
        .param("status",statusValue))
        .andExpect(status().isOk())
        .andReturn();
    MockHttpServletResponse response = result.getResponse();
    assertEquals(200, response.getStatus());
  }


  @Test
  void listOrderToPickUpWhenNotSuccessful() throws Exception {


    UUID deliveryManId = UUID.randomUUID();
    String statusValue = "Delivered";
    List<OrderEntity> orderEntities = new ArrayList<>();
    when(orderService.getOrdersByStatus(deliveryManId,statusValue)).thenReturn(orderEntities);
    MvcResult result = mockMvc.perform(get("/delivery/order/pickup/")
        .param("deliveryManId",deliveryManId.toString())
        .param("status",statusValue))
        .andExpect(status().is5xxServerError())
        .andReturn();
    MockHttpServletResponse response = result.getResponse();
    assertEquals(500, response.getStatus());
  }

  private Order buildOrder() {
    Order order = new Order();
    order.setId(UUID.randomUUID());
    order.setRestaurantName("KFC");
    order.setDeliveryAddress("address");
    order.setRestaurantAddress("restaurantAddress");
    order.setRestaurantArea("Central Zone");
    order.setEstimatedTimeOfCooking("2021-01-02T21:34:33.616Z");
    return order;
  }

  private OrderEntity buildOrderEntity() {
    OrderEntity orderEntity = new OrderEntity();
    orderEntity.setId(UUID.randomUUID());
    orderEntity.setDeliveryAddress("address");
    orderEntity.setRestaurantAddress("restaurantAddress");
    orderEntity.setRestaurantArea("Central Zone");
    orderEntity.setRestaurantName("KFC");
    return orderEntity;
  }


}


