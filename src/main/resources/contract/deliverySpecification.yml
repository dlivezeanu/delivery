openapi: 3.0.0
info:
  description: "This is a server used for the delivery API "
  version: "1.0.0"
  title: "Delivery API"
tags:
  - name: "Order"
    description: "Everything about orders."

paths:
  /delivery/order/pending:
    post:
      tags:
        - Order
      summary: "Creates a new order and sets its status to 'Pending preparation'"
      description: "It creates a new order and sets its status to 'pending' and it adds the estimate time of cooking "
      operationId: createOrderWithPendingStatus
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Order'

      responses:
        '200':
          description: Ok
        '400':
          description: Bad request.
        '409':
          description: Order already exists.

  /delivery/order/ready/{orderId}:
    put:
      tags:
        - Order
      summary: "Change order status to 'Pending delivery'"
      description: "It receives a id as a parameter and changes the status of the order based on that id"
      operationId: setOrderStatusToReady
      parameters:
        - name: orderId
          in: path
          description: Id of the order to be returned
          required: true
          schema:
            $ref: '#/components/schemas/Id'
      responses:
        '200':
          description: Ok
        '400':
          description: Bad request.
        '404':
          description: Not found.
        '409':
          description: Order already has the "ready" status



  /delivery/order/pickup:
    get:
      tags:
        - Order
      summary: "Retrieves a list of orders based on an their order state"
      description: "It retrieves a list of orders based on their order state"
      operationId: listOrderToPickUp
      parameters:
        - in: query
          name: deliveryManId
          required: true
          schema:
            $ref: '#/components/schemas/Id'
          description: The id of the delivery man.
        - in: query
          name: status
          required: false
          schema:
            type: string
          description: The status of the order.


      responses:
        '200':
          description: Returns the order array
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Order'

        '400':
          description: Bad request.



  /delivery/order/pickup/{orderId}:
    put:
      tags:
        - Order
      summary: "Changes the order status and estimated time of arrival based on its Id"
      description: "It changes the order status and estimated time of arrival based on its Id"
      operationId: setOrderStatusToPickup
      parameters:
        - name: orderId
          in: path
          description: Id of the order to be returned
          required: true
          schema:
            $ref: '#/components/schemas/Id'
        - name: deliveryManId
          in: query
          description: Id of the delivery man
          required: true
          schema:
            $ref: '#/components/schemas/DeliveryManId'
        - in: query
          name: estimatedTimeToDelivery
          schema:
            $ref: '#/components/schemas/TimeDate'
          description: The field for the estimated time to delivery.
      responses:
        '200':
          description: Ok
        '400':
          description: Bad request.
        '404':
          description: Not found.
        '409':
          description: Order already exists.
        '500':
          description: Internal server error

  /delivery/order/delivered/{orderId}:
    put:
      tags:
        - Order
      summary: "Change order status to 'delivered'"
      description: "It receives a id as a parameter and changes the status of the order based on that id"
      operationId: setOrderStatusToDelivered
      parameters:
        - name: orderId
          in: path
          description: Id of the order to change the status of.
          required: true
          schema:
            $ref: '#/components/schemas/Id'
      responses:
        '200':
          description: Ok
        '400':
          description: Bad request.
        '404':
          description: Not found.
        '409':
          description: Order already has the "delivered" status

  /delivery/order/{orderId}:
    get:
      tags:
        - Order
      summary: "Retrieves delivery estimated time based on an order id"
      description: "It retrieves delivery estimated time based on an order id"
      operationId: getDeliveryEstimatedTime
      parameters:
        - name: orderId
          in: path
          description: Id of the order to be returned
          required: true
          schema:
            $ref: '#/components/schemas/Id'

      responses:
        '200':
          description: Returns the estimated time
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TimeDate'

        '400':
          description: Bad request.
        '404':
          description: Not found.
        '500':
          description: Internal server error
components:
  schemas:
    Order:
      type: object
      properties:
        id:
          $ref: '#/components/schemas/Id'
        status:
          $ref: '#/components/schemas/Status'
        estimatedTimeOfCooking:
          $ref: '#/components/schemas/TimeDate'
        restaurantName:
          type: string
          default: Not initialized
        restaurantAddress:
          type: string
          default: Not initialized
        restaurantArea:
          type: string
          default: Not initialized
        deliveryAddress:
          type: string
          default: Not initialized


    Status:
      type: string
      enum:
        - Pending preparation
        - Pending delivery
        - In delivery
        - Delivered
    Id:
      type: string
      format: uuid

    DeliveryManId:
      type: string
      format: uuid

    TimeDate:
      type: string
      format: offset-date-time





