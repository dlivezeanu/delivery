package com.endava.delivery.controller;

import com.endava.delivery.entity.OrderEntity;
import com.endava.delivery.integration.feedback.payload.request.DeliveryManInfo;
import com.endava.delivery.model.Order;
import com.endava.delivery.service.OrderService;
import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class DeliveryController implements DeliveryApi {

  private OrderService orderService;
  private Mapper mapper = DozerBeanMapperBuilder.buildDefault();
  private RestTemplate restTemplate;

  @Autowired
  public DeliveryController(OrderService orderService,
      RestTemplate restTemplate) {
    this.orderService = orderService;
    this.restTemplate = restTemplate;
  }

  @Override
  public ResponseEntity<Void> createOrderWithPendingStatus(@Valid Order order) {
    OrderEntity orderEntity = mapper.map(order, OrderEntity.class);
    Optional<OrderEntity> orderToReturn = orderService.insertOrder(orderEntity);
    if (orderToReturn.isPresent()) {
      return new ResponseEntity<>(HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Override
  public ResponseEntity<Void> setOrderStatusToReady(UUID orderId) {
    Optional<OrderEntity> orderEntity = orderService.setOrderStatusToReady(orderId);
    if (orderEntity.isPresent()) {
      return new ResponseEntity<>(HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @Override
  public ResponseEntity<Void> setOrderStatusToDelivered(UUID orderId) {
    Optional<OrderEntity> orderEntity = orderService.setOrderStatusToDelivered(orderId);
    if (orderEntity.isPresent()) {
      return new ResponseEntity<>(HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @Override
  public ResponseEntity<List<Order>> listOrderToPickUp(@NotNull @Valid UUID deliveryManId,
      @Valid String statusValue) {

    List<OrderEntity> orderEntities;
    orderEntities = orderService.getOrdersByStatus(deliveryManId, statusValue);
    if (orderEntities.size() > 0) {
      return new ResponseEntity(orderEntities, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Override
  public ResponseEntity<Void> setOrderStatusToPickup(UUID orderId,
      @NotNull @Valid UUID deliveryManId,
      @Valid String estimatedTimeOfDelivery) {
    if (orderService
        .updateOrderWithEta(orderId, Integer.parseInt(estimatedTimeOfDelivery), deliveryManId)
        .isPresent()) {
      DeliveryManInfo deliveryManInfo = new DeliveryManInfo(orderId, deliveryManId);

      String url = "http://localhost:8082/feedback/order/ready";
      HttpHeaders httpHeaders = new HttpHeaders();
      httpHeaders.setContentType(MediaType.APPLICATION_JSON);
      HttpEntity<DeliveryManInfo> httpEntity = new HttpEntity<>(deliveryManInfo, httpHeaders);
      ResponseEntity<String> response =
          restTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
      if (response.getStatusCode() == HttpStatus.OK) {
        return new ResponseEntity<>(HttpStatus.OK);
      } else {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @Override
  public ResponseEntity<String> getDeliveryEstimatedTime(UUID orderId) {
    Optional<OrderEntity> orderEstimatedTime = orderService.getOrderEstimatedTime(orderId);
    return orderEstimatedTime.map(orderEntity -> ResponseEntity
        .ok(orderEntity.getEstimatedTimeOfArrival()))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.BAD_REQUEST));
  }

}
