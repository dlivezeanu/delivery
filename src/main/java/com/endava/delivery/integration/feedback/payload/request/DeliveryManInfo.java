package com.endava.delivery.integration.feedback.payload.request;

import java.util.UUID;

public class DeliveryManInfo {
  private UUID orderId;
  private UUID deliveryManId;

  public DeliveryManInfo(UUID orderId, UUID deliveryManId) {
    this.orderId = orderId;
    this.deliveryManId = deliveryManId;
  }

  public UUID getOrderId() {
    return orderId;
  }

  public void setOrderId(UUID orderId) {
    this.orderId = orderId;
  }

  public UUID getDeliveryManId() {
    return deliveryManId;
  }

  public void setDeliveryManId(UUID deliveryManId) {
    this.deliveryManId = deliveryManId;
  }
}
