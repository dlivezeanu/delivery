package com.endava.delivery.entity;

import com.endava.delivery.model.Status;
import java.util.UUID;
import nonapi.io.github.classgraph.json.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "order")
public class OrderEntity {

  @Id
  private UUID id;
  private UUID deliveryManId;
  private Status status;
  private String restaurantName;
  private String restaurantAddress;
  private String restaurantArea;
  private String deliveryAddress;
  private String estimatedTimeOfCooking;
  private String estimatedTimeOfArrival;


  public UUID getDeliveryManId() {
    return deliveryManId;
  }

  public void setDeliveryManId(UUID deliveryManId) {
    this.deliveryManId = deliveryManId;
  }

  public String getEstimatedTimeOfArrival() {
    return estimatedTimeOfArrival;
  }

  public void setEstimatedTimeOfArrival(String estimatedTimeOfArrival) {
    this.estimatedTimeOfArrival = estimatedTimeOfArrival;
  }

  public String getEstimatedTimeOfCooking() {
    return estimatedTimeOfCooking;
  }

  public void setEstimatedTimeOfCooking(String estimatedTimeOfCooking) {
    this.estimatedTimeOfCooking = estimatedTimeOfCooking;
  }



  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public String getRestaurantName() {
    return restaurantName;
  }

  public void setRestaurantName(String restaurantName) {
    this.restaurantName = restaurantName;
  }

  public String getRestaurantAddress() {
    return restaurantAddress;
  }

  public void setRestaurantAddress(String restaurantAddress) {
    this.restaurantAddress = restaurantAddress;
  }

  public String getRestaurantArea() {
    return restaurantArea;
  }

  public void setRestaurantArea(String restaurantZone) {
    this.restaurantArea = restaurantZone;
  }

  public String getDeliveryAddress() {
    return deliveryAddress;
  }

  public void setDeliveryAddress(String deliveryAddress) {
    this.deliveryAddress = deliveryAddress;
  }


}
