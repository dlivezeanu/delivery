package com.endava.delivery.repository;

import com.endava.delivery.entity.OrderEntity;
import java.util.UUID;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderRepository extends MongoRepository<OrderEntity, UUID> {

}
