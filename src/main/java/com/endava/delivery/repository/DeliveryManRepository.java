package com.endava.delivery.repository;

import com.endava.delivery.entity.DeliveryManEntity;
import java.util.UUID;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DeliveryManRepository extends MongoRepository<DeliveryManEntity, UUID> {

}
