package com.endava.delivery.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableWebMvc

public class WebMvcConfig extends WebMvcConfigurationSupport {
  @Override
  public FormattingConversionService mvcConversionService() {
    FormattingConversionService formattingConversionService = super.mvcConversionService();
    formattingConversionService.addConverter(new EnumConverter());
    return formattingConversionService;
  }
}

