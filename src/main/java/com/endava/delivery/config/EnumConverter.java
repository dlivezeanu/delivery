package com.endava.delivery.config;

import com.endava.delivery.model.Status;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.stereotype.Component;

@Component
@ReadingConverter
public class EnumConverter implements Converter<String, Status> {
  @Override
  public Status convert(String source) {
    try {
      return Status.fromValue(source);
    } catch (Exception exception) {
      return null;
    }
  }
}
