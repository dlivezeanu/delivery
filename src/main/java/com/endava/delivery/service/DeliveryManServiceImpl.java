package com.endava.delivery.service;


import com.endava.delivery.entity.DeliveryManEntity;
import com.endava.delivery.repository.DeliveryManRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeliveryManServiceImpl implements DeliveryManService {

  private DeliveryManRepository deliveryManRepository;

  @Autowired
  public DeliveryManServiceImpl(DeliveryManRepository deliveryManRepository) {
    this.deliveryManRepository = deliveryManRepository;
  }

  @Override
  public Optional<DeliveryManEntity> insertDeliveryMan(DeliveryManEntity deliveryManEntity) {
    return Optional.of(deliveryManRepository.insert(deliveryManEntity));
  }
}
