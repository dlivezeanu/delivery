package com.endava.delivery.service;

import com.endava.delivery.entity.OrderEntity;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OrderService {

  Optional<OrderEntity> insertOrder(OrderEntity orderEntity);

  Optional<OrderEntity> setOrderStatusToReady(UUID id);

  Optional<OrderEntity> setOrderStatusToDelivered(UUID id);

  Optional<OrderEntity> updateOrderWithEta(UUID orderId, int estimatedTimeOfArrival,
      UUID deliveryManId);

  Optional<OrderEntity> getOrderEstimatedTime(UUID orderId);

  List<OrderEntity> getOrdersByStatus(UUID deliveryManId, String status);


}
