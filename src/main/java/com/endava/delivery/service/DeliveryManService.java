package com.endava.delivery.service;

import com.endava.delivery.entity.DeliveryManEntity;
import java.util.Optional;

public interface DeliveryManService {

  Optional<DeliveryManEntity> insertDeliveryMan(DeliveryManEntity deliveryManEntity);

}
