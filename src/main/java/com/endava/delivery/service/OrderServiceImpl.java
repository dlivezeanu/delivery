package com.endava.delivery.service;

import com.endava.delivery.entity.DeliveryManEntity;
import com.endava.delivery.entity.OrderEntity;
import com.endava.delivery.model.Status;
import com.endava.delivery.repository.DeliveryManRepository;
import com.endava.delivery.repository.OrderRepository;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;


@Service
public class OrderServiceImpl implements OrderService {

  private OrderRepository orderRepository;
  private DeliveryManRepository deliveryManRepository;
  private MongoTemplate mongoTemplate;


  @Autowired
  public OrderServiceImpl(OrderRepository orderRepository, MongoTemplate mongoTemplate,
      DeliveryManRepository deliveryManRepository) {
    this.orderRepository = orderRepository;
    this.mongoTemplate = mongoTemplate;
    this.deliveryManRepository = deliveryManRepository;
  }

  @Override

  public Optional<OrderEntity> insertOrder(OrderEntity orderEntity) {
    orderEntity.setStatus(Status.PENDING_PREPARATION);
    return Optional.of(orderRepository.insert(orderEntity));
  }

  @Override

  public Optional<OrderEntity> setOrderStatusToReady(UUID id) {
    Optional<OrderEntity> orderToFind = orderRepository.findById(id);
    if (orderToFind.isPresent()) {
      orderToFind.get().setStatus(Status.PENDING_DELIVERY);
      return Optional.of(orderRepository.save(orderToFind.get()));
    } else {
      return Optional.empty();
    }
  }

  @Override
  public Optional<OrderEntity> setOrderStatusToDelivered(UUID id) {
    Optional<OrderEntity> orderToFind = orderRepository.findById(id);
    if (orderToFind.isPresent()) {
      orderToFind.get().setStatus(Status.DELIVERED);
      return Optional.of(orderRepository.save(orderToFind.get()));
    } else {
      return Optional.empty();
    }
  }

  @Override
  public Optional<OrderEntity> updateOrderWithEta(UUID orderId, int estimatedTimeOfArrival,
      UUID deliveryManId) {
    Optional<OrderEntity> orderEntity = orderRepository.findById(orderId);
    if (orderEntity.isPresent()) {
      orderEntity.get().setDeliveryManId(deliveryManId);
      orderEntity.get().setEstimatedTimeOfArrival(Instant.now()
          .plus(Duration.ofMinutes(estimatedTimeOfArrival)).toString());
      orderEntity.get().setStatus(Status.IN_DELIVERY);
      orderRepository.save(orderEntity.get());
      return orderEntity;
    } else {
      return Optional.empty();
    }
  }

  @Override
  public Optional<OrderEntity> getOrderEstimatedTime(UUID orderId) {
    Query query = new Query(Criteria.where("status").is(Status.IN_DELIVERY)
        .and("_id").is(orderId));
    return mongoTemplate.find(query, OrderEntity.class).stream().findFirst();
  }

  @Override
  public List<OrderEntity> getOrdersByStatus(UUID deliveryManId, String statusValue) {
    Optional<DeliveryManEntity> deliveryMan = deliveryManRepository.findById(deliveryManId);
    Criteria criteria = new Criteria();

    Status status;
    if (statusValue != null) {
      status = Status.fromValue(statusValue);
      if (status.toString().equals("Pending delivery")
          || status.toString().equals("Pending preparation")) {
        criteria.and("status").is(status);
      } else {
        return new ArrayList<>();
      }
    } else {
      criteria.and("status").in(Status.PENDING_DELIVERY, Status.PENDING_PREPARATION);
    }

    if (deliveryMan.isPresent()) {
      Query query = new Query(criteria.and("restaurantArea")
          .is(deliveryMan.get().getArea()));
      return mongoTemplate.find(query, OrderEntity.class);
    }
    return new ArrayList<>();
  }

}
